var _ = require("underscore");
var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var fs = require("fs");

//Variables
var urlimages = "http://192.168.16.106:8080/Images/Inmuebles/";

// colecciones BD
// P=propietario C=corredor I=interesados
var user = [
  { id: 0, user: "usuario0@gmail.com", pass: "usuario0", rol: "P", phone: "phone0", active: true },
  { id: 1, user: "usuario1@gmail.com", pass: "usuario1", rol: "I", phone: "phone1", active: true },
  { id: 2, user: "usuario2@gmail.com", pass: "usuario2", rol: "C", phone: "phone2", active: true },
  { id: 3, user: "usuario3@gmail.com", pass: "usuario3", rol: "P", phone: "phone3", active: true },
  { id: 4, user: "usuario4@gmail.com", pass: "usuario4", rol: "I", phone: "phone4", active: true },
  { id: 5, user: "usuario5@gmail.com", pass: "usuario5", rol: "C", phone: "phone5", active: true },
  { id: 6, user: "usuario6@gmail.com", pass: "usuario6", rol: "P", phone: "phone6", active: true },
  { id: 7, user: "usuario7@gmail.com", pass: "usuario7", rol: "I", phone: "phone7", active: true },
  { id: 8, user: "usuario8@gmail.com", pass: "usuario8", rol: "C", phone: "phone8", active: true },
  { id: 9, user: "usuario9@gmail.com", pass: "usuario9", rol: "P", phone: "phone9", active: true },
  
];

var inmuebles = [

  {
    id: 5498,
    name: "La Boyera",
    place: "La Boyera, Caracas - El Hatillo (norte), Distrito Capital",
    description: "Ubicada en calle cerrada",
    pics: ["a01.jpg","a02.jpg","a03.jpg","a04.jpg","a05.jpg","a06.jpg"],
    picsUrl: "http://tiendainmueble.com/images",
    price: 100000,
    group: "comercial",
    type: "apartamento",
    condition: "venta",
    state: "Miranda",
    municipality: "Cristobal Rojas",
    view3dpath: "https://tiendainmueble/3Ddirectory/5498.file",
    benefits: {"areat": 300,"areac": 200,"bedroom": 2,"bathroom": 2,"PartyRoom": 2,"laundryRoom": 2,"parking": 2,"antiquity": 10,"floors": 3},
    amenities: ["diningRoom","serviceRoom","meetingRoom","water","electricity","powerPlant","telephoneLine","gas","kitchen","garden","security","elevator","view3d"]
  },
  {
    id: 5345,
    name: "La Unión, Caracas - El Hatillo (norte)",
    place: "La Unión, Caracas - El Hatillo (norte), Distrito Capital",
    description: 'bella, ubicada en calle cerrada',
    pics: ["c01.jpg", "c02.jpg", "c03.jpg", "c04.jpg"],
    picsUrl: "http://tiendainmueble.com/images",
    price: 240000,
    group: "comercial",
    type: "casa",
    condition: "alquiler",
    state: "Falcon",
    municipality: "Union",
    view3dpath: "https://tiendainmueble/3Ddirectory/5498.file",
    benefits: {"areat": 300,"areac": 200,"bedroom": 2,"bathroom": 2,"PartyRoom": 2,"laundryRoom": 2,"parking": 2,"antiquity": 10,"floors": 3},
    amenities: ["diningRoom","serviceRoom","meetingRoom","water","electricity","powerPlant","telephoneLine","gas","kitchen","garden","security","elevator","view3d"]
  },
  {
    id: 4566,
    name: " Caracas - El Hatillo (norte)",
    place: "Calle 4, Caracas - El Hatillo (norte), Distrito Capital",
    description: 'bella, ubicada en calle cerrada',
    pics: ["c02.jpg", "c04.jpg", "c03.jpg","c01.jpg"],
    picsUrl: "http://tiendainmueble.com/images",
    price: 190000,
    group: "residencial",
    type: "casa",
    condition: "venta",
    state: "Lara",
    municipality: "Andres Eloy Blanco",
    view3dpath: "https://tiendainmueble/3Ddirectory/5498.file",
    benefits: {"areat": 300,"areac": 200,"bedroom": 2,"bathroom": 2,"PartyRoom": 2,"laundryRoom": 2,"parking": 2,"antiquity": 10,"floors": 3},
    amenities: ["diningRoom","serviceRoom","meetingRoom","water","electricity","powerPlant","telephoneLine","gas","kitchen","garden","security","elevator","view3d"]
  },
  {
    id: 7655,
    name: " Caracas - Sta Monica (norte)",
    place: "Av 4, Caracas - Sta Monica (norte), Distrito Capital",
    description: 'bella, ubicada en calle cerrada',
    pics: ["c03.jpg", "c02.jpg", "c03.jpg", "c04.jpg"],
    picsUrl: "http://tiendainmueble.com/images",
    price: 1850000,
    group: "comercial",
    type: "casa",
    condition: "arrendamiento",
    state: "Guarico",
    municipality: "El Socorro",
    view3dpath: "https://tiendainmueble/3Ddirectory/5498.file",
    benefits: {"areat": 300,"areac": 200,"bedroom": 2,"bathroom": 2,"PartyRoom": 2,"laundryRoom": 2,"parking": 2,"antiquity": 10,"floors": 3},
    amenities: ["diningRoom","serviceRoom","meetingRoom","water","electricity","powerPlant","telephoneLine","gas","kitchen","garden","security","elevator","view3d"]
  },
  {
    id: 7655,
    name: "Terreno Maracaibo",
    place: " Av. Bella Vista, Maracaibo Zulia",
    description: 'Amplia posibilidad de Agrandar Vivienda',
    pics: ["t01.jpg", "t02.jpg", "t03.jpg"],
    picsUrl: "http://tiendainmueble.com/images",
    price: 980000,
    group: "comercial",
    type: "terreno",
    condition: "venta",
    state: "Anzoategui",
    municipality: "Anaco",
    view3dpath: "https://tiendainmueble/3Ddirectory/5498.file",
    benefits: {"areat": 300,"areac": 200,"bedroom": 2,"bathroom": 2,"PartyRoom": 2,"laundryRoom": 2,"parking": 2,"antiquity": 10,"floors": 3},
    amenities: ["diningRoom","serviceRoom","meetingRoom","water","electricity","powerPlant","telephoneLine","gas","kitchen","garden","security","elevator","view3d"]
  },
  {
    id: 7655,
    name: "Quinta Crespo",
    place: " Quinta Crespo, Caracas - Libertador (centro), Distrito Capital",
    description: 'VENDO APTO EN OBRA GIS, IDEAL PARA REMODELAR A SU GUSTO, 2 HABITACIONES, 1 BAÑO, 1 PUESTO ESTACIONAMIENTO, AREAS DE RECREACION, SEGURIDAD 24 HORAS.',
    pics: ["t02.jpg", "t01.jpg", "t03.jpg"],
    picsUrl: "http://tiendainmueble.com/images",
    price: 980000,
    group: "comercial",
    type: "terreno",
    condition: "venta",
    state: "Barinas",
    municipality: "Sabaneta",
    view3dpath: "https://tiendainmueble/3Ddirectory/5498.file",
    benefits: {"areat": 300,"areac": 200,"bedroom": 2,"bathroom": 2,"PartyRoom": 2,"laundryRoom": 2,"parking": 2,"antiquity": 10,"floors": 3},
    amenities: ["diningRoom","serviceRoom","meetingRoom","water","electricity","powerPlant","telephoneLine","gas","kitchen","garden","security","elevator","view3d"]
  },
  {
    id: 7655,
    name: "Cumbres De Curumo",
    place: " Apartamentos en Venta en Cumbres De Curumo, Caracas",
    description: 'Bello, amplio y exclusivo apartamento ubicado en el sector mas tranquilo y residencial de Cumbres de Curumo, en edificio de tan solo 5 pisos y un apartamento por piso con ascensor privado. Tres cómodas habitaciones y tres baños, vestier en habitación principal, sala, comedor, bella cocina empotrada con tope de granito, maletero y dos puestos de estacionamiento propios, techados e independientes completan su excelente distribución. Calle de muy poco tránsito, conjunto pequeño con jardines y sala de fiesta como áreas sociales. Véalo, enamorese y hágalo suyo a precio de oportunidad!',
    pics: ["t03.jpg", "t02.jpg", "t03.jpg"],
    picsUrl: "http://tiendainmueble.com/images",
    price: 980000,
    group: "comercial",
    type: "terreno",
    condition: "venta",
    state: "Merida",
    municipality: "Alberto Adriani",
    view3dpath: "https://tiendainmueble/3Ddirectory/5498.file",
    benefits: {"areat": 300,"areac": 200,"bedroom": 2,"bathroom": 2,"PartyRoom": 2,"laundryRoom": 2,"parking": 2,"antiquity": 10,"floors": 3},
    amenities: ["diningRoom","serviceRoom","meetingRoom","water","electricity","powerPlant","telephoneLine","gas","kitchen","garden","security","elevator","view3d"]
  },

  

]

var citas = [
  {
    id: 0,
    idinmueble: 0,
    iduser: 0,
    dia1: "ss",
    dia2: "ss",
    dia3: "ss",
    hora1a: "",
    hora1b: "",
    hora1c: "",
    hora2a: "",
    hora2b: "",
    hora2c: "",
    hora3a: "",
    hora3b: "",
    hora3c: "",
    hora1aX: "",
    hora1bX: "",
    hora1cX: "",
    hora2aX: "",
    hora2bX: "",
    hora2cX: "",
    hora3aX: "",
    hora3bX: "",
    hora3cX: "",
    hora1aXX: "",
    hora1bXX: "",
    hora1cXX: "",
    hora2aXX: "",
    hora2bXX: "",
    hora2cXX: "",
    hora3aXX: "",
    hora3bXX: "",
    hora3cXX: "",

  }
];

var userSocket = {}

// dando acceso a Imagenes   http://192.168.16.106:8081/Images/Inmuebles/a01.jpg
app.use(express.static("public"));
app.use("/Images/Inmuebles", express.static(__dirname + "/Images/Inmuebles"));

app.use(bodyParser.json());                           
app.use(bodyParser.urlencoded({ extended: true }));   

let puerto = process.env.PORT || 3000 ;
var server = app.listen(puerto, function () {
  console.log("Example app listening at :%s", puerto);
});

// api imagenes  http://localhost:8081/image?id=0
app.get("/inmuebles", function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.type("application/json");
  try {
    // var fin = _.findWhere(inmuebles, { id: parseInt(req.query.id) });
    // var fin = inmuebles
    res.status(200).send(inmuebles); // responde array de datos de ese inmuebles
  } catch (error) {
    res.status(200).send({ error: true, message: "Incoherencia en datos " });
  }
});

// registra un usuario  http://192.168.16.106:8081/register?user=usuarioo&pass=clave&phone=mitlf
app.post("/register", function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.type("application/json");
  try {
    let body = req.body;
    var persona = {
      id: Date.now(),
      user: body.user,
      pass: body.pass,
      rol: "I",
      phone: body.phone,
      active: true
    };

    myfilter = { user: body.user };
    var fin = _.where(user, myfilter);

    if (fin.length !== 0) {
      // no sepuede registrar, ya existe
      console.log("registrado ya existe..");
      res.status(200).send({ ok: false, message: "ya existe el  usuario" }); // responde array de todos los datos de ese usuario
    } else {
      // proceder a registrar
      console.log("registrado usuario nuevo..");
      console.log('antes:',user);
      user.push(persona);
      console.log('despues:',user);
      res
        .status(200)
        .json({
          ok:true,
          message: "registrado nuevo usuario     ahora:" + user.length
        }); // responde array de todos los datos de ese usuario
    }
  } catch (error) {
    res.status(200).send({ ok: false, message: "Incoherencia en datos " });
  }
});

// confirmar usuario y pass http://localhost:8081/confirm?user=user03&pass=pass03
app.post("/confirm", function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.type("application/json");
  try {
    let body = req.body;
    myfilter = { user: body.user, pass: body.pass };
    console.log(myfilter);

    var fin = _.where(user, myfilter);

    if (fin.length>0) {
      res.status(200).json({ ok: true, message: "usuario registrado", payload: fin }); // responde array con todos los datos de ese usuario
      
    } else {
      res.status(200).json({ ok: false, message: "Incoherencia en datos " });
      
    }

  } catch (error) {
    res.status(200).json({  ok: false, message: "Incoherencia en datos " });
  }
});

// para ver si hay conexion
app.get('/test', function (req, res) {
  res.json({
      ok: true,
      message: 'si hay conexion con el servidor..'
  });
});




global.usuarioConectado = false;
global.io = require('socket.io')(server);
global.io.on("connection", socket => {
  console.log('------------------------------------------------------------');
  console.log('socket.id :',socket.id);
  global.usuarioConectado= true;
  console.log('------------------------------------------------------------');
});

























// AGREGAR usuario COMPLETO http://localhost:8081/adduser?user=usernn&pass=passnn&rol=P&phone=phonenn
// app.get("/adduser", function (req, res) {
//   console.log(req);
//   res.setHeader("Access-Control-Allow-Origin", "*");
//   res.type("application/json");
//   try {
//     myfilter = { usbodyuery.user };
//     var fin = _.where(user, myfilter);
//     if (fin.length === 0) {
//       //{ id: 0, user: "user00", pass: "pass00", rol: "P", phone: "phone00" },

//       var mi_usuario = {
//         id: user.length,
//         user: req.query.user,
//         pass: req.query.pass,
//         rol: req.query.rol,
//         phone: req.query.phone
//       };
//       let antes = user.length;
//       user.push(mi_usuario);
//       let despues = user.length;
//       res.status(200).send({ error: false, message: "Registrado" });
//       console.log(
//         "Agregado un usuario, antes habian " + antes + " ahora hay " + despues
//       );
//     } else {
//       res.status(200).send({
//         error: true,
//         message: "Usuario No registrado, faltan datos" + fin.length
//       });
//     }
//   } catch (error) {
//     res.status(200).send({
//       error: true,
//       message: "Incoherencia en datos " + error.message + fin.length
//     });
//   }
// });

// api default
app.get("/listUsers", function (req, res) {
  fs.readFile(__dirname + "/" + "users.json", "utf8", function (err, data) {
    console.log(data);
    console.log("consulta hecha.." + _.now());
    res.end(data);
  });
  res.status(200).send({ jfjfjf: "dffffffffffffffffffffffffffffffffffff" });
});

// api imagenes  http://localhost:8100/image?id=0
app.get("/image", function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.type("application/json");
  try {
    var fin = _.findWhere(inmuebles, { id: parseInt(req.query.id) });
    res.status(200).send(fin); // responde array de datos de ese inmuebles
  } catch (error) {
    res.status(200).send({ error: true, message: "Incoherencia en datos " });
  }
});


// api user trae toda la inf del usuario  http://192.168.16.106:8081/user?id=3
app.get("/user", function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.type("application/json");
  try {
    var fin = _.findWhere(user, { id: parseInt(req.query.id) });
    res.status(200).send(fin); // responde array de todos los datos de ese usuario
  } catch (error) {
    res.status(200).send({ error: true, message: "Incoherencia en datos " });
  }
});

// confirmar usuario y pass http://localhost:8081/confirm?user=user03&pass=pass03
app.get("/confirm", function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.type("application/json");
  try {
    myfilter = { user: req.query.user, pass: req.query.pass };
    var fin = _.where(user, myfilter);
    res.status(200).send(fin); // responde array con todos los datos de ese usuario
  } catch (error) {
    res.status(200).send({ error: true, message: "Incoherencia en datos " });
  }
});


// borra un usuario  http://localhost:8081/deleteuser?id=1
app.get("/deleteuser", function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.type("application/json");
  try {
    var fin = _.findWhere(user, { id: parseInt(req.query.id) });

    //  arr = _.without(arr, _.findWhere(arr, { id: 3 }));
    var finSinUsuario = _.without(
      user,
      _.findWhere(user, { id: parseInt(req.query.id) })
    );
    user = finSinUsuario;
    console.log(user);
    res.status(200).send(fin); // responde array de todos los datos de ese usuario
  } catch (error) {
    res.status(200).send({ error: true, message: "Incoherencia en datos " });
  }
});

// registra un usuario  http://192.168.16.106:8081/register?user=usuarioo&pass=clave&phone=mitlf
app.get("/register", function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.type("application/json");
  try {
    var persona = {
      id: user.length,
      user: req.query.user,
      pass: req.query.pass,
      rol: "I",
      phone: req.query.phone,
      active: true
    };
    console.log(JSON.stringify(persona));

    myfilter = { user: req.query.user };
    var fin = _.where(user, myfilter);

    if (fin.length !== 0) {
      // no sepuede registrar, ya existe
      console.log("registrado ya existe..");
      res.status(200).send({ error: true, message: "ya existe el  usuario" }); // responde array de todos los datos de ese usuario
    } else {
      // proceder a registrar
      user.push(persona);
      console.log(JSON.stringify(user));
      console.log("registrado usuario nuevo..");
      res
        .status(200)
        .send({
          error: false,
          message: "registrado nuevo usuario     ahora:" + user.length
        }); // responde array de todos los datos de ese usuario
    }
  } catch (error) {
    res.status(200).send({ error: true, message: "Incoherencia en datos " });
  }
});

// POST http://localhost:8081/api/users
// parameters sent with
app.get("/api/users", function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.type("application/json");
  res.send("holaaaaaaaaaaaaaa");
});
